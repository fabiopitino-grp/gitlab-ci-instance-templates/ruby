# Ruby

Provides common settings for a standard Ruby app. You need to customize the configuration according to your needs:

For example:

1. Include or not `rubocop` job.
1. Select the right services that your app uses.
1. Use `rspec` job if you use RSpec framework, otherwise create your own test job.
1. Customize the `deploy` job to use your app provider.

## Example

```yaml
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Ruby.gitlab-ci.yml

# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/ruby/tags/
image: ruby:latest

# Pick zero or more services to be used on all builds.
# Only needed when using a docker container to run your tests in.
# Check out: http://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service
services:
  - mysql:latest
  - redis:latest
  - postgres:latest

variables:
  POSTGRES_DB: database_name

# Cache gems in between builds
cache:
  paths:
    - vendor/ruby

# This is a basic example for a gem or script which doesn't use
# services such as redis or postgres
before_script:
  - ruby -v  # Print out ruby version for debugging
  # Uncomment next line if your rails app needs a JS runtime:
  # - apt-get update -q && apt-get install nodejs -yqq
  - bundle config set path 'vendor'  # Install dependencies into ./vendor/ruby
  - bundle install -j $(nproc)

# Optional - Delete if not using `rubocop`
rubocop:
  script:
    - rubocop

rspec:
  script:
    - rspec spec

rails:
  variables:
    DATABASE_URL: "postgresql://postgres:postgres@postgres:5432/$POSTGRES_DB"
  script:
    - rails db:migrate
    - rails db:seed
    - rails test

# This deploy job uses a simple deploy flow to Heroku, other providers, e.g. AWS Elastic Beanstalk
# are supported too: https://github.com/travis-ci/dpl
deploy:
  type: deploy
  environment: production
  script:
    - gem install dpl
    - dpl --provider=heroku --app=$HEROKU_APP_NAME --api-key=$HEROKU_PRODUCTION_KEY

```
